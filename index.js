const express = require('express')

const puppeteer = require('puppeteer')

const app = express()
// const fs = require("fs")
var index = 0
app.post('/pdf/export', (req, res) => {
    const captureScreenshot = async () => {
        const browser = await puppeteer.launch({
            args: [
                '--no-sandbox',
                '--disable-setuid-sandbox',
                // '--font-render-hinting=none'
            ]
        })

        const page = await browser.newPage()
        // const html = `./template_html.txt`
        // let data = null;
        // data = await fs.readFileSync(html, "utf8")

        // let textPage = 'data:text/html,' + data

        let res = await page.goto('https://short.ink/eA-mUIYNT',
            { waitUntil: 'networkidle0' })

        
        let data = await page.content()
        console.log("==============================")
        console.log(data)

        index++
        return data
    }

    captureScreenshot()
    // res.download(`${__dirname}/data/viblo-asia.pdf`)
    res.send('ok')
})

app.listen(3012, () => {
    console.log("server running")
})